import { getOrgByName, Outway, Outways } from "./organizations";
import { CustomWorld } from "../support/custom-world";
import { strict as assert } from "assert";

export const hasOutwayRunning = async (
  world: CustomWorld,
  orgName: string,
  outwayName: string
) => {
  const outways = await getOutways(orgName);

  assert.equal(!!outways[outwayName], true);
};

export const getOutways = async (orgName: string): Promise<Outways> => {
  const org = getOrgByName(orgName);

  const outwaysResponse =
    await org.apiClients.management?.managementListOutways();

  const outways = outwaysResponse?.outways;
  outways?.forEach((outway) => {
    if (!outway.name) {
      return;
    }

    org.outways[`${outway.name}`].name = outway.name || "";
    org.outways[`${outway.name}`].publicKeyPEM = outway.publicKeyPEM || "";
    org.outways[`${outway.name}`].publicKeyFingerprint =
      outway.publicKeyFingerprint || "";
  });

  return org.outways;
};

export const getOutwayByName = async (
  orgName: string,
  outwayName: string
): Promise<Outway> => {
  const outways = await getOutways(orgName);

  return outways[outwayName];
};
