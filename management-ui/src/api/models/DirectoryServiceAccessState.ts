/* tslint:disable */
/* eslint-disable */
/**
 * management.proto
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: version not set
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ManagementAccessProof,
    ManagementAccessProofFromJSON,
    ManagementAccessProofFromJSONTyped,
    ManagementAccessProofToJSON,
} from './ManagementAccessProof';
import {
    ManagementOutgoingAccessRequest,
    ManagementOutgoingAccessRequestFromJSON,
    ManagementOutgoingAccessRequestFromJSONTyped,
    ManagementOutgoingAccessRequestToJSON,
} from './ManagementOutgoingAccessRequest';

/**
 * 
 * @export
 * @interface DirectoryServiceAccessState
 */
export interface DirectoryServiceAccessState {
    /**
     * 
     * @type {ManagementOutgoingAccessRequest}
     * @memberof DirectoryServiceAccessState
     */
    accessRequest?: ManagementOutgoingAccessRequest;
    /**
     * 
     * @type {ManagementAccessProof}
     * @memberof DirectoryServiceAccessState
     */
    accessProof?: ManagementAccessProof;
}

export function DirectoryServiceAccessStateFromJSON(json: any): DirectoryServiceAccessState {
    return DirectoryServiceAccessStateFromJSONTyped(json, false);
}

export function DirectoryServiceAccessStateFromJSONTyped(json: any, ignoreDiscriminator: boolean): DirectoryServiceAccessState {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'accessRequest': !exists(json, 'accessRequest') ? undefined : ManagementOutgoingAccessRequestFromJSON(json['accessRequest']),
        'accessProof': !exists(json, 'accessProof') ? undefined : ManagementAccessProofFromJSON(json['accessProof']),
    };
}

export function DirectoryServiceAccessStateToJSON(value?: DirectoryServiceAccessState | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'accessRequest': ManagementOutgoingAccessRequestToJSON(value.accessRequest),
        'accessProof': ManagementAccessProofToJSON(value.accessProof),
    };
}

